#include "TrustMe_GameDev.h"
#include "GravityWorldSettings.h"

float AGravityWorldSettings::GetGravityZ() const
{
	if( bInvertGravity )
	{
		return Super::GetGravityZ() * -1;
	}
	return Super::GetGravityZ();
}
