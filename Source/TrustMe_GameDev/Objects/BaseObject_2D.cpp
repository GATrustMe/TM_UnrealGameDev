#include "TrustMe_GameDev.h"
#include "BaseObject_2D.h"

ABaseObject_2D::ABaseObject_2D()
{
	// Main Configs
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = false;
	bReplicateMovement = false;

	// Create Components
	BoxColliderComponent = CreateDefaultSubobject<UBoxComponent>( TEXT( "BoxCollider" ) );
	SpriteComponent = CreateDefaultSubobject<UPaperSpriteComponent>( TEXT( "Sprite" ) );

	// Configuration
	BoxColliderComponent->SetBoxExtent( FVector( 38.f, 2.5f, 38.f ), false );
	BoxColliderComponent->SetSimulatePhysics( true );

	// Default Assets
	const ConstructorHelpers::FObjectFinder<UPaperSprite> DefaultSprite( TEXT( "/Paper2D/DummySprite" ) );
	SpriteComponent->SetSprite( DefaultSprite.Object );

	// Collision Settings
	SpriteComponent->SetCollisionProfileName( "NoCollision" );

	// Attachment
	RootComponent = BoxColliderComponent;
	SpriteComponent->AttachTo( RootComponent );
}
