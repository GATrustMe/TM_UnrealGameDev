#pragma once

#include "GameFramework/Actor.h"
#include "BaseObject_2D.generated.h"

UCLASS()
class TRUSTME_GAMEDEV_API ABaseObject_2D : public AActor
{
	GENERATED_BODY()

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = Collider, meta = ( AllowPrivateAccess = "true" ) )
	UBoxComponent* BoxColliderComponent;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = Sprite, meta = ( AllowPrivateAccess = "true" ) )
	UPaperSpriteComponent* SpriteComponent;
	
public:	
	ABaseObject_2D();
	
	FORCEINLINE UPaperSpriteComponent* GetSprite()
	{
		return SpriteComponent;
	}

	FORCEINLINE UBoxComponent* GetBoxCollider()
	{
		return BoxColliderComponent;
	}
};
