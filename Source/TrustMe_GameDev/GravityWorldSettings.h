#pragma once

#include "GameFramework/WorldSettings.h"
#include "GravityWorldSettings.generated.h"

UCLASS()
class TRUSTME_GAMEDEV_API AGravityWorldSettings : public AWorldSettings
{
	GENERATED_BODY()

public:
	virtual float GetGravityZ() const override;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = World )
	uint32 bInvertGravity : 1;
};
