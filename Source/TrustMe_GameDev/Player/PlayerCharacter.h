#pragma once

#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class TRUSTME_GAMEDEV_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

	FName					GrabTag;
	FVector					GrabDistance;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, meta = ( AllowPrivateAccess = "true" ) )
	UPhysicsHandleComponent* PhysicHandle;

public:
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Controlls" )
	uint32					bInvertedControlls : 1;

protected:
	UPROPERTY( BlueprintReadWrite, Category = "Grab" )
	AActor*		GrabTarget;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Grab" )
	float		GrabRange;

	UPROPERTY( BlueprintReadWrite, Category = "Grab" )
	bool		bCanGrab;

public:
	APlayerCharacter();
	
	virtual void Tick( float DeltaSeconds ) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	FORCEINLINE UPhysicsHandleComponent* GetPhysicHandle()
	{
		return PhysicHandle;
	}

private:
	void MoveRight( float Value );

protected:
	UFUNCTION( BlueprintCallable, Category = "Grab" )
	void Grab();

	UFUNCTION( BlueprintCallable, Category = "Grab" )
	void Release();
};
