#include "TrustMe_GameDev.h"
#include "PlayerCharacter.h"

APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	PhysicHandle = CreateDefaultSubobject<UPhysicsHandleComponent>( TEXT( "PhysicGrab" ) );

	PhysicHandle->LinearDamping = 700.f;
	PhysicHandle->LinearStiffness = 200.f;
	PhysicHandle->AngularDamping = 2000.f;
	PhysicHandle->AngularStiffness = 200.f;

	GrabTag = FName( "GrabAble" );

	GrabRange = 100.f;
	GrabDistance = FVector::ZeroVector;

	GrabTarget = nullptr;
	bCanGrab = true;
	bInvertedControlls = false;
}

void APlayerCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if( GrabTarget != nullptr )
	{
		PhysicHandle->SetTargetLocation( GetActorLocation() + GrabDistance );
	}
}

void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAction( "Jump", IE_Pressed, this, &ACharacter::Jump );

	InputComponent->BindAxis( "Right", this, &APlayerCharacter::MoveRight );
}

void APlayerCharacter::MoveRight( float Value )
{
	AddMovementInput( FVector( 0.f, (bInvertedControlls)?(-1.f):(1.f), 0.f ), Value );
}

void APlayerCharacter::Grab()
{
	if( bCanGrab && GrabTarget == nullptr )
	{
		GetWorld()->DebugDrawTraceTag = GrabTag;

		FHitResult HitResult;
		FCollisionQueryParams TraceParams( GrabTag, false, this );

		FVector TraceStart = GetActorLocation() - FVector( 0.f, 0.f, 15.f );
		FVector TraceEnd = TraceStart + GetActorRotation().Vector().GetUnsafeNormal() * GrabRange;

		if( GetWorld()->LineTraceSingleByChannel( HitResult, TraceStart, TraceEnd, ECC_WorldDynamic, TraceParams ) )
		{
			if( HitResult.GetActor()->ActorHasTag( GrabTag ) )
			{
				GrabTarget = HitResult.GetActor();

				GrabDistance = GrabTarget->GetActorLocation() - ( GetActorLocation() - FVector( 0.f, GetCapsuleComponent()->GetScaledCapsuleRadius(), 0.f ) );

				PhysicHandle->GrabComponent( HitResult.GetComponent(), HitResult.BoneName, HitResult.Location, false );
			}
		}
	}
}

void APlayerCharacter::Release()
{
	if( bCanGrab && GrabTarget != nullptr )
	{
		PhysicHandle->ReleaseComponent();
		Cast<UPrimitiveComponent>( GrabTarget->GetRootComponent() )->WakeAllRigidBodies();
		
		GrabTarget = nullptr;
	}
}
